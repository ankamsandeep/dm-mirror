#!/bin/bash
set -e

#TOKEN="__SET_YOUR_PERSONAL_ACCESS_TOKEN__"
#PROJECT_ID="__SET_NUMERIC_PROJECT_ID__"
TOKEN="glpat-MtjiwvYzBj3nkVRxUq7_"
PROJECT_ID="42494949"
GITLAB_INSTANCE="https://gitlab.com/api/v4/projects"

PAGE=1
while true; do
    PIPELINES=$(curl --header "PRIVATE-TOKEN: $TOKEN" "$GITLAB_INSTANCE/$PROJECT_ID/pipelines?per_page=100&page=$PAGE" | jq '.[] | select(.status == "failed") | .id')
    if [ -z "$PIPELINES" ]; then
        break
    fi
    for PIPELINE in $PIPELINES; do
        echo "Deleting pipeline $PIPELINE"
        curl --header "PRIVATE-TOKEN: $TOKEN" --request "DELETE" "$GITLAB_INSTANCE/$PROJECT_ID/pipelines/$PIPELINE"
    done
    PAGE=$((PAGE + 1))
done
