#!/bin/bash

# Get the GitLab API variables
PROJECT_ID="$CI_PROJECT_ID"
PRIVATE_TOKEN="$TOKEN"

# Get the current running pipeline ID
CURRENT_PIPELINE=$(curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/pipelines?status=running" | jq '.[0].id')

# Get the IDs of all the running jobs for the pipeline that match the job names "test", "build", and "deploy"
RUNNING_JOB_IDS_ARRAY=($(curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/pipelines/$CURRENT_PIPELINE/jobs?status=running" | jq -r '.[] | select(.name | contains("test") or contains("build") or contains("deploy")) | .id'))

# Loop over all the running jobs and generate links to their artifacts
ARTIFACTS_LINKS=""
for JOB_ID in "${RUNNING_JOB_IDS_ARRAY[@]}"; do
    ARTIFACTS_LINK="[Artifacts for Job $JOB_ID](https://gitlab.com/$CI_PROJECT_PATH/-/jobs/$JOB_ID/artifacts/browse)"
    ARTIFACTS_LINKS="$ARTIFACTS_LINKS $ARTIFACTS_LINK<br><br>"
done

# Get the current date and time
DATE=$(date)

# Check if a release exists for this pipeline
RELEASE=$(curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/releases/tags/$CI_COMMIT_TAG")
if [ -z "$RELEASE" ]; then
    # No release found, so only update the wiki page with the artifacts links
    NEW_WIKI_CONTENT="Links to current artifacts as of $DATE <br><br> $ARTIFACTS_LINKS<br><br>"
else
    # Release found, so generate the release link and update the wiki page with both the artifacts and release links
    RELEASE_LINK="[Release for tag $CI_COMMIT_TAG](https://gitlab.com/$CI_PROJECT_PATH/-/releases/$CI_COMMIT_TAG)"
    NEW_WIKI_CONTENT="Links to current artifacts as of $DATE <br><br> $ARTIFACTS_LINKS<br><br>$RELEASE_LINK<br><br>"
fi

# Get the current content of the wiki page
WIKI_CONTENT=$(curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/wikis/home" | jq -r '.content')

# Append the links to the wiki content and update the wiki page
UPDATED_WIKI_CONTENT="$WIKI_CONTENT<br><br>$NEW_WIKI_CONTENT<br><br>"
curl --request PUT --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" --data "content=$UPDATED_WIKI_CONTENT" "https://gitlab.com/api/v4/projects/$PROJECT_ID/wikis/home"
