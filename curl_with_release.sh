#!/bin/bash

PROJECT_ID="$CI_PROJECT_ID"
PRIVATE_TOKEN="$TOKEN"

# Get the current running pipeline ID
CURRENT_PIPELINE=$(curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/pipelines?status=running" | jq '.[0].id')

# Get the IDs of all the running jobs for the pipeline that match the job names "test" and "build" and "deploy"
RUNNING_JOB_IDS_ARRAY=($(curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/pipelines/$CURRENT_PIPELINE/jobs?status=running" | jq -r '.[] | select(.name | contains("test") or contains("build") or contains("deploy")) | .id'))

# Get the current release tag
RELEASE_TAG=$(curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/repository/tags?order_by=updated" | jq -r '.[0].name')

# Loop over all the running jobs and generate links to their artifacts
ARTIFACT_LINKS=""
for JOB_ID in "${RUNNING_JOB_IDS_ARRAY[@]}"; do
    ARTIFACT_LINK="[Artifacts for Job $JOB_ID](https://gitlab.com/ankamsandeep/artifacts-demo/-/jobs/$JOB_ID/artifacts/browse)"
    ARTIFACT_LINKS="$ARTIFACT_LINKS $ARTIFACT_LINK<br><br>"
done

# Get the current date and time
DATE=$(date)

# Get the current content of the wiki page
WIKI_CONTENT=$(curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/wikis/home" | jq -r '.content')

# Append the links and release tag to the wiki content and update the wiki page
NEW_WIKI_CONTENT="$WIKI_CONTENT<br><br>Links to current artifacts as of $DATE (Release Tag: $RELEASE_TAG):<br><br>$ARTIFACT_LINKS<br><br>"
curl --request PUT --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" --data "content=$NEW_WIKI_CONTENT" "https://gitlab.com/api/v4/projects/$PROJECT_ID/wikis/home"
